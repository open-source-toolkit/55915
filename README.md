# Genymotion ARM 翻译工具集合

## 简介

本仓库提供了一个名为 `Genymotion_ARM_Translation.rar` 的资源文件下载。该文件包含了多个版本的 Genymotion ARM 翻译工具，适用于不同的 Android 版本。通过使用这些工具，您可以在 Genymotion 虚拟设备上运行 ARM 架构的应用程序。

## 文件内容

下载并解压 `Genymotion_ARM_Translation.rar` 后，您将获得以下文件：

- `Genymotion-ARM-Translation_for_4.3.zip`
- `Genymotion-ARM-Translation_for_4.4.zip`
- `Genymotion-ARM-Translation_for_5.1.zip`
- `Genymotion-ARM-Translation_for_6.0.zip`
- `Genymotion-ARM-Translation_for_7.X.zip`
- `Genymotion-ARM-Translation_for_8.0.zip`

## 使用方法

1. **下载并解压**：首先下载 `Genymotion_ARM_Translation.rar` 文件，并将其解压到您的本地目录。

2. **选择合适的版本**：根据您使用的 Genymotion 虚拟设备的 Android 版本，选择相应的 `Genymotion-ARM-Translation_for_X.X.zip` 文件。

3. **应用翻译工具**：将选定的 `.zip` 文件拖放到正在运行的 Genymotion 虚拟设备窗口中。虚拟设备将自动安装并应用该工具。

4. **重启虚拟设备**：安装完成后，重启 Genymotion 虚拟设备以使更改生效。

## 注意事项

- 请确保您选择的 `Genymotion-ARM-Translation_for_X.X.zip` 文件与您的虚拟设备 Android 版本匹配，否则可能会导致兼容性问题。
- 在使用这些工具之前，建议备份您的虚拟设备数据，以防出现意外情况。

## 贡献

如果您有任何改进建议或发现了新的 ARM 翻译工具，欢迎提交 Pull Request 或 Issue。

## 许可证

本仓库中的资源文件遵循开源许可证，具体信息请参考文件中的许可证声明。

---

希望这些工具能帮助您在 Genymotion 虚拟设备上顺利运行 ARM 架构的应用程序！